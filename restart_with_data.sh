#!/usr/bin/env bash

export IS_RESTART=true

./restart.sh
python manage.py loaddata initial_data/db.json
cp -r initial_data/media media
