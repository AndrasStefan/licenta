#!/usr/bin/env bash

export IS_RESTART=true

rm db.sqlite3
rm -rf core/migrations
rm -rf media
python manage.py makemigrations
python manage.py makemigrations core
python manage.py migrate
python manage.py restart
