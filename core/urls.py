from django.contrib.auth import views as auth_views
from django.urls import path
from django.views.generic import RedirectView

from core import views

urlpatterns = [
    path('', RedirectView.as_view(url='expressions'), name='index'),
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('register', views.register, name='register'),
    path('expressions', views.all_expressions, name='all_expressions'),
    path('add_expressions', views.add_expression, name='add_expression'),
    path('delete_expression<int:pk>', views.delete_expression, name='delete_expression'),
    path('search_expression', views.search_expression, name='search_expression'),
]
