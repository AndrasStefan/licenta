from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, get_object_or_404

from core.forms import DrawingForm
from core.models import Expression


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required
def all_expressions(request):
    return render(request, 'expressions.html', {
        'exps': Expression.objects.filter(author=request.user)
    })


@login_required
def add_expression(request):
    if request.method == 'POST':
        form = DrawingForm(request.POST)
        if form.is_valid():
            image = form.cleaned_data.get('image')
            name = form.cleaned_data.get('name')
            Expression.objects.create(
                author=request.user, name=name, image=Expression.base64_file(image)
            )
            return redirect('all_expressions')
    else:
        form = DrawingForm()

    return render(request, "add_expression.html", {
        'form': form,
    })


@login_required
def delete_expression(request, pk):
    expr = get_object_or_404(Expression, pk=pk)
    expr.delete()
    return redirect('all_expressions')


@login_required
def search_expression(request):
    query = request.GET.get('q')
    return render(request, 'expressions.html', {
        'exps': Expression.objects.filter(author=request.user, name__icontains=query)
    })


@login_required
def index(request):
    return render(request, 'index.html')
