from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import IntegrityError

from core.models import Expression


class Command(BaseCommand):
    help = "Restart the state of database."

    @staticmethod
    def _create_admin():
        try:
            User.objects.create_superuser(username="admin", password="admin", email="admin@yahoo.com")
        except IntegrityError:
            pass

    @staticmethod
    def _delete_expressions():
        Expression.objects.all().delete()

    def handle(self, *args, **options):
        self._create_admin()
        self._delete_expressions()
        self.stdout.write(self.style.SUCCESS('Done.'))
