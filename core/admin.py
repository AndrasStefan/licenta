from django.contrib import admin
from django.contrib.auth.models import Group
from django.utils.html import format_html

from core.models import Digit, Expression, class_number_to_symbol

admin.site.unregister(Group)


@admin.register(Digit)
class DigitAdmin(admin.ModelAdmin):
    readonly_fields = ('proc_image', 'big_img', 'predicted', 'get_percents_as_string')
    exclude = ('processed_image', 'big_image', 'predictions')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def proc_image(self, obj):
        return format_html('<img src="{}" />'.format(obj.processed_image.url))

    def big_img(self, obj):
        return format_html('<img src="{}" />'.format(obj.big_image.url))

    proc_image.short_description = 'Processed Digit'
    big_img.short_description = 'Resized Digit (for debug only)'


class DigitInline(admin.TabularInline):
    model = Digit
    extra = 0

    readonly_fields = (
        'proc_image', 'big_img', 'p0', 'p1', 'p2', 'p3', 'p4', 'p5',
        'p6', 'p7', 'p8', 'p9', 'p10', 'p11', 'predicted'
    )
    exclude = ('processed_image', 'big_image', 'predictions')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def p0(self, obj):
        return obj.get_prediction_value(0)

    def p1(self, obj):
        return obj.get_prediction_value(1)

    def p2(self, obj):
        return obj.get_prediction_value(2)

    def p3(self, obj):
        return obj.get_prediction_value(3)

    def p4(self, obj):
        return obj.get_prediction_value(4)

    def p5(self, obj):
        return obj.get_prediction_value(5)

    def p6(self, obj):
        return obj.get_prediction_value(6)

    def p7(self, obj):
        return obj.get_prediction_value(7)

    def p8(self, obj):
        return obj.get_prediction_value(8)

    def p9(self, obj):
        return obj.get_prediction_value(9)

    def p10(self, obj):
        return obj.get_prediction_value(10)

    def p11(self, obj):
        return obj.get_prediction_value(11)

    def proc_image(self, obj):
        return format_html('<img src="{}" />'.format(obj.processed_image.url))

    def big_img(self, obj):
        return format_html('<img src="{}" />'.format(obj.big_image.url))

    proc_image.short_description = 'Processed Digit'
    big_img.short_description = 'Resized Digit (for debug only)'
    for i, elem in enumerate([p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11]):
        setattr(elem, 'short_description', class_number_to_symbol.get(i))
    p10.short_description = '+'


@admin.register(Expression)
class ExpressionAdmin(admin.ModelAdmin):
    readonly_fields = ('image_tag', 'predicted')
    exclude = ('image',)
    list_display = ('name', 'author', 'image', 'predicted')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def image_tag(self, obj):
        return format_html('<img src="{}" />'.format(obj.image.url))

    inlines = [
        DigitInline,
    ]
