from django import forms


class DrawingForm(forms.Form):
    name = forms.CharField(required=False)
    image = forms.CharField(widget=forms.HiddenInput())
