import os

import numpy as np
from PIL import Image
from PIL.ImageOps import invert

class_name = 'equal'
class_number = 11

train_path = f'{class_name}_augmented'
test_path = f'{class_name}_test_augmented'

x_train = None
y_train = None

for i, filename in enumerate(os.listdir(train_path)):
    path = os.path.join(train_path, filename)
    img = invert(Image.open(path))
    result = np.array(img).astype('uint8')

    if x_train is None:
        x_train = result.reshape(1, 28, 28)
    else:
        x_train = np.append(x_train, result.reshape(1, 28, 28), axis=0)

    if not i % 100:
        print(f'Iteration: {i}')

y_train = np.full((x_train.shape[0],), class_number, dtype='uint8')

# ----------------------------------------------------

x_test = None
y_test = None

for i, filename in enumerate(os.listdir(test_path)):
    path = os.path.join(test_path, filename)
    img = invert(Image.open(path))
    result = np.array(img).astype('uint8')

    if x_test is None:
        x_test = result.reshape(1, 28, 28)
    else:
        x_test = np.append(x_test, result.reshape(1, 28, 28), axis=0)

    if not i % 100:
        print(f'Iteration: {i}')

y_test = np.full((x_test.shape[0],), class_number, dtype='uint8')

np.savez(
    f'{class_name}_sign',
    x_train=x_train,
    x_test=x_test,
    y_train=y_train,
    y_test=y_test,
)
