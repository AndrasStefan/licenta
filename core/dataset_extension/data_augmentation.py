from keras_preprocessing.image import ImageDataGenerator

class_name = 'equal'
class_symbol = '='
test = True

if test:
    save_to_dir_path = f'{class_name}_test_augmented'
    iterations = 50
else:
    save_to_dir_path = f'{class_name}_augmented'
    iterations = 200


image_generator = ImageDataGenerator(
            rotation_range=10,
            zoom_range=0.05,
            horizontal_flip=True,
            vertical_flip=True,
            data_format="channels_last"
)

foo = image_generator.flow_from_directory(
    directory=f'{class_name}_images_by_hand',
    target_size=(28, 28),
    color_mode='grayscale',
    classes=class_symbol,
    class_mode=None,
    save_to_dir=save_to_dir_path,
    save_format='png',
)

i = 0
for x_batch in foo:
    i += 1
    if i > iterations:
        break
