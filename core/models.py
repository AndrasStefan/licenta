import base64
from io import BytesIO

import numpy as np
from PIL import Image, ImageFilter
from PIL.ImageOps import invert
from django.conf import settings
from django.core.files.base import ContentFile
from django.db import models
from skimage.measure import find_contours

from licenta.settings import CNN, KNN

class_number_to_symbol = {
    0: '0', 1: '1', 2: '2',
    3: '3', 4: '4', 5: '5',
    6: '6', 7: '7', 8: '8',
    9: '9', 10: '+', 11: '='
}


def image_to_content_file(path=None, image=None, name='image.png'):
    if path is None and image is None:
        raise ValueError("You need to provide a path or an image")

    if path is not None:
        image = Image.open(path)

    foo = BytesIO()
    image.save(foo, format='PNG')
    return ContentFile(foo.getvalue(), name=name)


class Digit(models.Model):
    processed_image = models.ImageField(upload_to='proc_images')
    big_image = models.ImageField(upload_to='big_images')
    predicted = models.CharField(max_length=10, blank=True)
    predictions = models.CharField(max_length=500, blank=True)

    expression = models.ForeignKey(
        'Expression',
        on_delete=models.CASCADE,
        null=True, blank=True
    )

    def get_prediction_value(self, digit):
        if self.predictions:
            return format(float(self.predictions.split()[digit]), '.2%')

        return None

    @property
    def get_percents_as_string(self):
        return '\n'.join(
            [f'{class_number_to_symbol.get(i)} - {format(float(elem), ".2%")}' for i, elem in enumerate(
                self.predictions.split()
            )]
        )

    @staticmethod
    def predict(image):
        im = Image.open(image)
        im = invert(im)

        input = np.array(im).astype('float32')
        input /= 255
        
        if KNN:
            from licenta.settings import knn_model
            input = input.reshape(1, -1)

            rez = knn_model.predict_proba(input)
            return class_number_to_symbol.get(np.argmax(rez)), rez[0]
        elif CNN:
            from licenta.settings import cnn_model
            input = input.reshape(1, 28, 28, 1)

            rez = cnn_model.predict(input)
            return class_number_to_symbol.get(np.argmax(rez)), rez[0]

    @staticmethod
    def resize_image(image_field):
        image = Image.open(image_field).resize((64, 64))
        return image_to_content_file(image=image, name=image_field.name)

    def save(self, *args, **kwargs):
        self.predicted, percentages = self.predict(self.processed_image)
        self.predictions = ' '.join(str(elem) for elem in percentages)
        self.big_image = self.resize_image(self.processed_image)
        super(Digit, self).save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.processed_image.name)


class Expression(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=True)
    image = models.ImageField(upload_to='images')

    class Meta:
        ordering = ['-pk']

    @staticmethod
    def base64_file(data, name=None):
        _format, _img_str = data.split(';base64,')
        _name, ext = _format.split('/')
        if not name:
            name = _name.split(":")[-1]
        return ContentFile(base64.b64decode(_img_str), name='{}.{}'.format(name, ext))

    @property
    def predicted(self):
        return "".join(elem.predicted for elem in self.digit_set.all())

    @property
    def evaluate(self):
        """
        :return: 0 - corect
                 1 - gresit
                 2 - a aparut o eroare
        """
        value = self.predicted
        equal_position = value.find("=")
        expression = value[:equal_position] + '=' + value[equal_position:]

        try:
            if eval(expression):
                return 0
            else:
                return 1
        except Exception:
            return 2

    @staticmethod
    def fit_to_28_by_28(image):
        width, height = float(image.size[0]), float(image.size[1])
        new_image = Image.new('L', (28, 28), 255)

        if width > height:
            new_height = int(round((20.0 / width * height), 0))
            image = image.resize((20, new_height), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
            top = int(round(((28 - new_height) / 2), 0))
            new_image.paste(image, (4, top))
        else:
            new_width = int(round((20.0 / height * width), 0))
            image = image.resize((new_width, 20), Image.ANTIALIAS).filter(ImageFilter.SHARPEN)
            left = int(round(((28 - new_width) / 2), 0))
            new_image.paste(image, (left, 4))

        return new_image

    @staticmethod
    def clean_boxes(boxes):
        rez = [boxes[0]]

        def is_inside_box(a, b):
            """
            :return: True if B is inside A
            """
            x_min_a, x_max_a, y_min_a, y_max_a = a
            x_min_b, x_max_b, y_min_b, y_max_b = b

            return x_min_a < x_min_b and x_max_a > x_max_b and y_min_a < y_min_b and y_max_a > y_max_b

        def one_below_the_other(a, b):
            x_min_a, x_max_a, y_min_a, y_max_a = a
            x_min_b, x_max_b, y_min_b, y_max_b = b

            return abs(y_min_a - y_min_b) + abs(y_max_a - y_max_b) < 80

        def concatenate_two_boxes(a, b):
            x_min_a, x_max_a, y_min_a, y_max_a = a
            x_min_b, x_max_b, y_min_b, y_max_b = b

            x_min_rez = min(x_min_a, x_min_b)
            x_max_rez = max(x_max_a, x_max_b)
            y_min_rez = min(y_min_a, y_min_b)
            y_max_rez = max(y_max_a, y_max_b)

            return x_min_rez, x_max_rez, y_min_rez, y_max_rez

        for elem in boxes[1:]:
            last = rez[-1]
            if is_inside_box(last, elem):
                continue

            if one_below_the_other(last, elem):
                rez.pop()
                rez.append(concatenate_two_boxes(last, elem))
                continue

            rez.append(elem)

        return rez

    def preprocess(self, input):
        img = Image.open(input).convert("L")
        image = np.array(img)

        contours = find_contours(image, 0.8)
        boxes = []

        for contour in contours:
            x_min = int(np.min(contour[:, 0]))
            x_max = int(np.max(contour[:, 0]))
            y_min = int(np.min(contour[:, 1]))
            y_max = int(np.max(contour[:, 1]))
            boxes.append((x_min, x_max, y_min, y_max))

        if not boxes:
            self.delete()
            return

        boxes.sort(key=lambda tup: tup[2])

        for x_min, x_max, y_min, y_max in self.clean_boxes(boxes):
            cropped = image[x_min:x_max, y_min:y_max]

            img = Image.fromarray(cropped)
            output = self.fit_to_28_by_28(img)

            Digit.objects.create(
                processed_image=image_to_content_file(image=output, name=input.name),
                expression=self
            )

    def save(self, *args, **kwargs):
        super(Expression, self).save(*args, **kwargs)

        if not self.name:
            self.name = self.image.name.split('/')[-1]
            self.save()
        else:
            self.preprocess(self.image)

    def __str__(self):
        return '{} - {}'.format(self.name, self.image)
