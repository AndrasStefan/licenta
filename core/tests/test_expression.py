import os

import pytest

from core.models import Expression, Digit, image_to_content_file
from licenta.settings import BASE_DIR

images = os.path.join(BASE_DIR, 'core', 'tests', 'images')
pytestmark = pytest.mark.django_db


@pytest.mark.parametrize('image_name, digit, count', [
    ('image_0.png', '0', 5),
    ('image_1.png', '1', 9),
    ('image_2.png', '2', 5),
    ('image_3.png', '3', 6),
    ('image_4.png', '4', 4),
    ('image_5.png', '5', 6),
    ('image_6.png', '6', 6),
    ('image_7.png', '7', 5),
    ('image_8.png', '8', 5),
    ('image_9.png', '9', 5),
    ('image_plus.png', '+', 6),
    ('image_=.png', '=', 5),
])
def test_classify_one_digit_multiple_times(
        settings, tmp_path, image_name, digit, count, admin_user
):
    settings.MEDIA_ROOT = tmp_path
    image_path = os.path.join(images, image_name)

    exp = Expression.objects.create(
        author=admin_user, image=image_to_content_file(path=image_path)
    )

    print('*' * 30)
    print(image_name, digit)
    for dig in exp.digit_set.all():
        print(dig.get_percents_as_string)
        print('-' * 10)

    assert Expression.objects.count() == 1
    assert Digit.objects.count() == count

    assert Expression.objects.first().predicted == digit * count
