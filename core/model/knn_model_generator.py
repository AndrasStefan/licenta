# -*- coding: utf-8 -*-
"""Untitled0.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/15tlFP6OuqBcZwfWhZNgYZH5P46jLFBUW
"""

from keras.datasets import mnist
from sklearn.neighbors import KNeighborsClassifier
import numpy as np

(x_train, y_train), (x_test, y_test) = mnist.load_data()

path = 'plus_sign.npz'

with np.load(path, allow_pickle=True) as f:
    aug_x_train, aug_y_train = f['x_train'], f['y_train']
    aug_x_test, aug_y_test = f['x_test'], f['y_test']

x_train = np.append(x_train, aug_x_train, axis=0)
y_train = np.append(y_train, aug_y_train, axis=0)

x_test = np.append(x_test, aug_x_test, axis=0)
y_test = np.append(y_test, aug_y_test, axis=0)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

train_samples, train_1, train_2 = x_train.shape
x_train = x_train.reshape((train_samples, train_1 * train_2))

test_samples, test_1, test_2 = x_test.shape
x_test = x_test.reshape((test_samples, test_1 * test_2))

knn = KNeighborsClassifier(
    n_neighbors=6,
    algorithm='kd_tree',
    weights='distance',
    n_jobs=-1,
    leaf_size=40,
    p=2,
)
print("Before fit")
knn.fit(x_train, y_train)
print("After fit")

print(knn.score(x_test, y_test))

print("Before dump")
from sklearn.externals import joblib
joblib.dump(knn, 'plus_knn_model.pkl')
print("After dump")

model = joblib.load('plus_knn_model.pkl')
print(model.score(x_test, y_test))
