# -*- coding: utf-8 -*-
"""cnn_generator.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1VSv0vm83IK2OjfRbMEO9HZXBxiFkGdYI
"""

import keras
from keras.utils import plot_model
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

batch_size = 128
num_classes = 12
epochs = 15

img_rows, img_cols = 28, 28

(x_train, y_train), (x_test, y_test) = mnist.load_data()

import numpy as np
from google.colab import drive
drive.mount('/content/gdrive')

path = 'gdrive/My Drive/plus_sign.npz'

with np.load(path, allow_pickle=True) as f: 
  aug_x_train, aug_y_train = f['x_train'], f['y_train'] 
  aug_x_test, aug_y_test = f['x_test'], f['y_test']
  
x_train = np.append(x_train, aug_x_train, axis=0)
y_train = np.append(y_train, aug_y_train, axis=0)

x_test = np.append(x_test, aug_x_test, axis=0)
y_test = np.append(y_test, aug_y_test, axis=0)

print(x_train.shape, y_train.shape, x_test.shape, y_test.shape)

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()

model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.4))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, 
          verbose=1, 
          validation_data=(x_test, y_test)
)

score = model.evaluate(x_test, y_test, verbose=1)

print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save('cnn_model.h5')

from google.colab import files
files.download('cnn_model.h5')

from keras.models import load_model
model2 = load_model('cnn_model.h5')

score = model2.evaluate(x_test, y_test, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

import matplotlib.pyplot as plt
from numpy import argmax

input = x_test[13]

print(x_test.shape)
print(input.shape)
plt.imshow(input.reshape(28, 28))

pred = model.predict(input.reshape(1, img_rows, img_cols, 1))
print(pred)
print(argmax(pred))

!pip freeze